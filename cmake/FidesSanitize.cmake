#=========================================================================
#
#  This software is distributed WITHOUT ANY WARRANTY; without even
#  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#  PURPOSE.  See the above copyright notice for more information.
#
#=========================================================================

# This code has been adapted from remus (https://gitlab.kitware.com/cmb/remus)

if(CMAKE_CXX_COMPILER_ID STREQUAL "Clang" OR
   CMAKE_CXX_COMPILER_ID STREQUAL "AppleClang")
  set(CMAKE_COMPILER_IS_CLANGXX 1)
endif()

if(CMAKE_COMPILER_IS_GNUCXX OR CMAKE_COMPILER_IS_CLANGXX)

  #Add option for enabling sanitizers
  option(FIDES_ENABLE_SANITIZER "Build with sanitizer support." OFF)
  mark_as_advanced(FIDES_ENABLE_SANITIZER)

  if(FIDES_ENABLE_SANITIZER)
    set(FIDES_SANITIZER "address"
      CACHE STRING "The sanitizer to use")
    mark_as_advanced(FIDES_SANITIZER)

    #We're setting the CXX flags and C flags beacuse they're propagated down
    #independent of build type.
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -fsanitize=${FIDES_SANITIZER}")
    set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -fsanitize=${FIDES_SANITIZER}")
    set(CMAKE_EXE_LINKER_FLAGS "${CMAKE_EXE_LINKER_FLAGS} -fsanitize=${FIDES_SANITIZER}")
    set(CMAKE_SHARED_LINKER_FLAGS "${CMAKE_SHARED_LINKER_FLAGS} -fsanitize=${FIDES_SANITIZER}")
  endif()
endif()

